package com.mm.giphytrending.listing.repository.network

import com.google.gson.annotations.SerializedName


data class TrendingResponse
(
        @SerializedName("data")
        var data: List<GifItem>?
)

data class GifItem(
        @SerializedName("id")
        var id: String?,
        @SerializedName("url")
        var url: String?,
        @SerializedName("username")
        var userName: String?,
        @SerializedName("title")
        var title: String?,
        @SerializedName("images")
        var images: ItemImages?
) {
    data class ItemImages(
            @SerializedName("preview_gif")
            var previewGif: Image?,
            @SerializedName("downsized")
            var downsizedGif: Image?
    ) {
        data class Image(
                @SerializedName("url")
                var url: String
        )
    }
}