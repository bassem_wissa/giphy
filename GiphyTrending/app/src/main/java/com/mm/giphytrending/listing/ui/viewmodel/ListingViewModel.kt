package com.mm.giphytrending.listing.ui.viewmodel


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.mm.giphytrending.listing.ui.model.GifUiItem
import com.mm.giphytrending.listing.ui.model.ListingModel
import com.mm.giphytrending.listing.ui.model.onError
import com.mm.giphytrending.listing.ui.model.onSuccess
import com.mm.giphytrending.utils.NetworkStateHelper
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import kotlin.coroutines.experimental.CoroutineContext


class ListingViewModel(private val listingModel: ListingModel, private val networkStateHelper: NetworkStateHelper,
                       private val apiKey: String)
    : ViewModel() {
    val gifItems: MutableLiveData<List<GifUiItem>> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val showNoInternet: MutableLiveData<Boolean> = MutableLiveData()
    val isLoadingMore: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    private var pageSize: Int = 20
    private var offset: Int = 0
    private var itemsJob: Job = Job()
    private var allItems = ArrayList<GifUiItem>()

    fun requestItems(coroutineContext: CoroutineContext) {
        // check for internet
        if (!networkStateHelper.hasInternet()) {
            showNoInternet.value = true
            showNoInternet.value = false
            return
        }
        if (isLoading.value == true) {
            return
        }
        if (isLoadingMore.value == true) {
            return
        }

        launch(coroutineContext + itemsJob) {
            if (offset == 0) {
                isLoading.value = true
            } else {
                isLoadingMore.value = true
            }
            listingModel.getTrendingGifs(apiKey, pageSize, offset)
                    .onSuccess {
                        offset += it.size
                        allItems.addAll(it)
                        gifItems.value = it
                        hideLoading()
                    }
                    .onError {
                        errorMessage.value = it.localizedMessage
                        hideLoading()
                    }
        }
    }

    fun refreshItems(coroutineContext: CoroutineContext) {
        allItems.clear()
        gifItems.value = ArrayList()
        offset = 0
        requestItems(coroutineContext)
    }

    override fun onCleared() {
        super.onCleared()
        itemsJob.cancel()
    }

    private fun hideLoading() {
        isLoading.value = false
        isLoadingMore.value = false
    }

    fun getOffset(): Int {
        return offset
    }

    fun restoreOriginalItems() {
        gifItems.value = allItems
    }

}