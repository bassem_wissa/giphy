package com.mm.giphytrending.listing.domain

import com.mm.giphytrending.listing.repository.TrendingRepository
import com.mm.giphytrending.listing.repository.network.GifItem
import com.mm.giphytrending.listing.repository.network.onError
import com.mm.giphytrending.listing.repository.network.onSuccess


interface GetTrendingGifsUseCase {
    suspend fun getTrendingGifs(apiKey: String,
                                pageSize: Int,
                                offset: Int): TrendingGifsResult<List<GifImageItem>>
}

class GetTrendingGifsUseCaseImpl(private val repository: TrendingRepository) : GetTrendingGifsUseCase {
    override suspend fun getTrendingGifs(apiKey: String,
                                         pageSize: Int,
                                         offset: Int): TrendingGifsResult<List<GifImageItem>> {
        var items: ArrayList<GifImageItem>? = null
        var throwable: Throwable? = null
        val res=repository.getTrendingGifs(apiKey,pageSize,offset)
        val x=res.toString()
        repository.getTrendingGifs(apiKey,
                pageSize,
                offset)
                .onSuccess { trendingResponse ->
                    items = ArrayList()
                    items?.addAll(trendingResponse.data?.map { it.toGifImageItem() } ?: ArrayList())

                }
                .onError {
                    throwable = it
                }
        items?.let { return Success(it) }
        throwable?.let { return Failure(it) }
        return Failure(Error("Could not load items"))

    }

    private fun GifItem.toGifImageItem(): GifImageItem {
        val previewUrl = this.images?.previewGif?.url
        val gifUrl = this.images?.downsizedGif?.url
        val webUrl = this.url
        val userName = this.userName
        return GifImageItem(previewUrl, gifUrl, webUrl, userName)
    }
}
