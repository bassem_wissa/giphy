# Giphy Trending gifs
## App Description:
The app displays an endless list of gif from Giphy's public trending [api](https://developers.giphy.com/dashboard/?create=true)
And when the user clicks on a gif file it should be able to view it in full screen

## Technical Description:
* As requested the implementation was not supposed to be done in more than 4 hours so what have been implemented so far was done 5.5 hours, if I have stopped by 4 hours there won't have been a detail view for the gif, orientation and tests
* The app is structured in Clean with ViewModel from the architecture components for the presentation layer.
* The code is in Kotlin
* I have used Dagger2 to inject dependencies in the app.
* I have added  unit tests to test the repository, domain and presentation layers.
* 3rd party libraries used: Dagger2, Retrofit2, [Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines.html) for asynchronous code with addition to [Anko's coroutines extensions](https://github.com/Kotlin/anko/wiki/Anko-Coroutines) and Glide.
* A working release apk for the app can be downloaded from [here](https://www.dropbox.com/s/b79dksbujowgesn/gif-app-release.apk?dl=0)

## Please note:
The app would run fast on later devices in debug mode but if in release mode it runs fast in all device ranges ( due to Glide configuration when loading gif files in debug vs release builds)

## TODO:
* Implement endless scrolling behavior using the paging library from architecture components as a proof of concept
* Enhancing current UI and if more time available implementing the "stretch" requirements.