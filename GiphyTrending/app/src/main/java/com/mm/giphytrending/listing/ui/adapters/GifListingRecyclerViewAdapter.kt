package com.mm.giphytrending.listing.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mm.giphytrending.R
import com.mm.giphytrending.listing.ui.model.GifUiItem
import com.mm.giphytrending.utils.image.ImageLoader
import kotlinx.android.synthetic.main.gif_list_item.view.*


class GifListingRecyclerViewAdapter(val imageLoader: ImageLoader, var onItemClicked: (GifUiItem?) -> Unit) : RecyclerView.Adapter<GifListingRecyclerViewAdapter.ViewHolder>() {
    private var dataSet = ArrayList<GifUiItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.gif_list_item, parent, false)
        return ViewHolder(view, {
            onItemClicked(dataSet?.get(it))
        })
    }

    override fun getItemCount(): Int {
        return dataSet?.size ?: 0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(dataSet?.get(position), imageLoader)
    }

    fun addItems(items: List<GifUiItem>) {
        dataSet.addAll(items)
    }

    fun clearItems() {
        dataSet?.clear()
    }

    class ViewHolder(view: View, onItemClicked: (Int) -> Unit) : RecyclerView.ViewHolder(view) {

        init {
            view.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }

        fun bind(item: GifUiItem?, imageLoader: ImageLoader) {
            imageLoader.loadImageUrl(itemView.image_view,
                    item?.previewUrl,
                    R.drawable.gif_place_holder)
            itemView.txt_user_name.text = item?.userName ?: ""
        }
    }
}