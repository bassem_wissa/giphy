package com.mm.giphytrending.application

import android.app.Application
import com.mm.giphytrending.application.di.ApplicationComponent
import com.mm.giphytrending.application.di.ApplicationModule
import com.mm.giphytrending.application.di.DaggerApplicationComponent
import com.mm.giphytrending.utils.Constants


class TrendingApplication : Application() {
    private var applicationComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
    }

    private fun initAppComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this, Constants.BASE_URL,Constants.GIPHY_API_KEY))
                .build()
    }

    fun getApplicationComponent(): ApplicationComponent? {
        return this.applicationComponent
    }

    companion object {
        fun get(application: Application): TrendingApplication {
            return application as TrendingApplication
        }
    }
}