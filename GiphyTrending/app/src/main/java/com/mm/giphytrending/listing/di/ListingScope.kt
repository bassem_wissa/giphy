package com.mm.giphytrending.listing.di

import javax.inject.Scope

@Scope
internal annotation class ListingScope
