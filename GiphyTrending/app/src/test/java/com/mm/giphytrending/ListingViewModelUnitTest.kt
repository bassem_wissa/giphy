package com.mm.giphytrending

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.mm.giphytrending.listing.ui.model.GifUiItem
import com.mm.giphytrending.listing.ui.model.ListingModel
import com.mm.giphytrending.listing.ui.model.ListingResult
import com.mm.giphytrending.listing.ui.viewmodel.ListingViewModel
import com.mm.giphytrending.utils.CoroutineContextProvider
import com.mm.giphytrending.utils.NetworkStateHelper
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.runners.MockitoJUnitRunner
import org.junit.Assert.assertEquals


@RunWith(MockitoJUnitRunner::class)
class ListingViewModelUnitTest {
    lateinit var viewModel: ListingViewModel
    @Mock
    lateinit var networkStateHelper: NetworkStateHelper

    @Mock
    lateinit var itemsObserver: Observer<List<GifUiItem>>
    @Mock
    lateinit var isLoadingObserver: Observer<Boolean>
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        val model = object : ListingModel {
            override suspend fun getTrendingGifs(apiKey: String, pageSize: Int, offset: Int): ListingResult<List<GifUiItem>> {
                return com.mm.giphytrending.listing.ui.model.Success(
                        getPopulatedUiItemsList()
                )
            }
        }

        viewModel = ListingViewModel(model, networkStateHelper, "")
    }

    @Test
    fun testViewModel() {
        Mockito.`when`(networkStateHelper.hasInternet()).thenReturn(true)

        viewModel.gifItems.observeForever(itemsObserver)
        viewModel.requestItems(CoroutineContextProvider.IO().get())
        verify(itemsObserver).onChanged(getPopulatedUiItemsList())

    }




    private fun getPopulatedUiItemsList(): List<GifUiItem> {
        return ArrayList<GifUiItem>().apply {
            add(GifUiItem("previewUrl", "gifUrl", "webUrl", "userName"))
            add(GifUiItem("previewUrl2", "gifUrl2", "webUrl2", "userName2"))
        }
    }
}