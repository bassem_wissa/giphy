package com.mm.giphytrending

import com.mm.giphytrending.listing.repository.TrendingRepository
import com.mm.giphytrending.listing.repository.TrendingRepositoryImpl
import com.mm.giphytrending.listing.repository.network.*
import kotlinx.coroutines.experimental.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.Response
import retrofit2.mock.Calls


@RunWith(MockitoJUnitRunner::class)
class RepositoryUnitTest {
    private lateinit var trendingRepository: TrendingRepository
    @Mock
    lateinit var trendingService: TrendingService

    @Before
    fun setup() {
        trendingRepository = TrendingRepositoryImpl(trendingService)
    }

    @Test
    fun testTrendingRepository() = runBlocking {

        val trendResult: NetworkResult<TrendingResponse> = Success(getTrendingResponse())
        Mockito.`when`(trendingService.getTrendingGifs("", 2, 0)).thenReturn(getMockedCall())

        val repoResult = trendingRepository.getTrendingGifs("", 2, 0)
        assertEquals(repoResult, trendResult)


    }

    @Test
    fun testTrendingRepositoryFailure() = runBlocking {

        Mockito.`when`(trendingService.getTrendingGifs("", 2, 0))
                .thenReturn(Calls.response(Response.error(500, ResponseBody.create(null, "Some error"))))
        val repoResult = trendingRepository.getTrendingGifs("", 2, 0)
        assertEquals(repoResult.toString().trim(), Failure(Error("Trending response is null")).toString().trim())

    }

    private fun getMockedCall(): Call<TrendingResponse>? {
        return Calls.response(getTrendingResponse())
    }

    private fun getTrendingResponse(): TrendingResponse {
        return TrendingResponse(
                data = getListOfImages()
        )

    }

    private fun getListOfImages(): List<GifItem> {
        return ArrayList<GifItem>().apply {
            add(GifItem("id1",
                    "someUrl",
                    "myUserName",
                    "someTitle",
                    GifItem.ItemImages(
                            GifItem.ItemImages.Image("previewGif"),
                            GifItem.ItemImages.Image("downsizedGif")
                    )))
            add(GifItem("id2",
                    "someUrl2",
                    "myUserName2",
                    "someTitle2",
                    GifItem.ItemImages(
                            GifItem.ItemImages.Image("previewGif2"),
                            GifItem.ItemImages.Image("downsizedGif2")
                    )))
        }
    }


}
