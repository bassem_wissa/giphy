package com.mm.giphytrending.listing.repository

import com.mm.giphytrending.listing.repository.network.*
import kotlinx.coroutines.experimental.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.experimental.suspendCoroutine


interface TrendingRepository {
    suspend fun getTrendingGifs(apiKey: String,
                                pageSize: Int,
                                offset: Int): NetworkResult<TrendingResponse>
}

class TrendingRepositoryImpl(private val trendingService: TrendingService) : TrendingRepository {
    override suspend fun getTrendingGifs(apiKey: String, pageSize: Int, offset: Int): NetworkResult<TrendingResponse> {
        return suspendCoroutine { continuation ->
            trendingService.getTrendingGifs(apiKey,
                    pageSize,
                    offset)
                    .enqueue(object : Callback<TrendingResponse> {
                        override fun onFailure(call: Call<TrendingResponse>?, t: Throwable?) {
                            continuation.resume(Failure(Error(t?.message
                                    ?: "Failed to load trending")))
                        }

                        override fun onResponse(call: Call<TrendingResponse>?, response: Response<TrendingResponse>?) {
                            val trends = response?.body()
                            if (trends != null) {
                                trends.let {
                                    continuation.resume(Success(it))

                                }
                            } else {
                                continuation.resume(Failure(Error("Trending response is null")))
                            }
                        }
                    })

        }
    }
}


