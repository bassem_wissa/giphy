package com.mm.giphytrending.listing.ui.model

import android.os.Parcel
import android.os.Parcelable


data class GifUiItem(var previewUrl: String?,
                     var gifUrl: String?,
                     var webUrl: String?,
                     var userName: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(previewUrl)
        parcel.writeString(gifUrl)
        parcel.writeString(webUrl)
        parcel.writeString(userName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GifUiItem> {
        override fun createFromParcel(parcel: Parcel): GifUiItem {
            return GifUiItem(parcel)
        }

        override fun newArray(size: Int): Array<GifUiItem?> {
            return arrayOfNulls(size)
        }
    }
}