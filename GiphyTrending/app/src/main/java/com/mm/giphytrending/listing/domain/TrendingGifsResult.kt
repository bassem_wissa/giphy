package com.mm.giphytrending.listing.domain


sealed class TrendingGifsResult
<out T : Any>

data class Success<out T : Any>(val data: T) : TrendingGifsResult<T>()
data class Failure(val error: Throwable?) : TrendingGifsResult<Nothing>()

inline fun <T : Any> TrendingGifsResult<T>.onSuccess(action: (T) -> Unit): TrendingGifsResult<T> {
    if (this is Success) action(data)
    return this
}

inline fun <T : Any> TrendingGifsResult<T>.onError(action: (Throwable) -> Unit) {
    if (this is Failure && error != null) action(error)
}
