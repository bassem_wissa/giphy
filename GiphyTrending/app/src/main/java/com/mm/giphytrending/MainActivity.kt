package com.mm.giphytrending

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.SyncStateContract
import android.widget.Toast
import com.mm.giphytrending.application.TrendingApplication
import com.mm.giphytrending.listing.di.ListingModule
import com.mm.giphytrending.listing.domain.GetTrendingGifsUseCaseImpl
import com.mm.giphytrending.listing.domain.onError
import com.mm.giphytrending.listing.domain.onSuccess
import com.mm.giphytrending.listing.repository.TrendingRepositoryImpl
import com.mm.giphytrending.listing.repository.network.*
import com.mm.giphytrending.listing.ui.GifDialogFragment
import com.mm.giphytrending.listing.ui.GifListingFragment
import com.mm.giphytrending.listing.ui.model.GifUiItem
import com.mm.giphytrending.listing.ui.viewmodel.ListingViewModel
import com.mm.giphytrending.listing.ui.viewmodel.ListingViewModelFactory
import com.mm.giphytrending.utils.Constants
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.experimental.CoroutineContext
import kotlin.coroutines.experimental.suspendCoroutine

class MainActivity : AppCompatActivity(), GifListingFragment.OnFragmentInteractionListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeContent()
    }

    private fun initializeContent() {
        if (supportFragmentManager.findFragmentByTag(GifListingFragment.TAG) == null) {
            supportFragmentManager.beginTransaction().replace(R.id.frm_content,
                    GifListingFragment.newInstance(),
                    GifListingFragment.TAG)
                    .commit()
        }
    }

    override fun onItemClicked(gifUiItem: GifUiItem) {
        GifDialogFragment.newInstance(gifUiItem)
                .show(supportFragmentManager,GifDialogFragment.TAG)
    }

}
