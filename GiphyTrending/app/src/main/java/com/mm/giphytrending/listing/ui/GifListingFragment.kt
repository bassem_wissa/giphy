package com.mm.giphytrending.listing.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mm.giphytrending.R
import com.mm.giphytrending.application.TrendingApplication
import com.mm.giphytrending.listing.di.ListingModule
import com.mm.giphytrending.listing.ui.adapters.GifListingRecyclerViewAdapter
import com.mm.giphytrending.listing.ui.model.GifUiItem
import com.mm.giphytrending.listing.ui.viewmodel.ListingViewModel
import com.mm.giphytrending.listing.ui.viewmodel.ListingViewModelFactory
import com.mm.giphytrending.utils.CoroutineContextProvider
import com.mm.giphytrending.utils.image.ImageLoader
import com.mm.giphytrending.utils.ui.LoadMoreRecyclerView
import kotlinx.android.synthetic.main.fragment_gif_listing.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [GifListingFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [GifListingFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class GifListingFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ListingViewModelFactory
    lateinit var viewModel: ListingViewModel
    @Inject
    lateinit var imageLoader: ImageLoader

    private var listener: OnFragmentInteractionListener? = null
    private var rvAdapter: GifListingRecyclerViewAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeDependencies()
        initializeRecyclerView()
        observeData()
        viewModel.requestItems(CoroutineContextProvider.Main().get())
        swipe_refresh_main.setOnRefreshListener {
            rvAdapter?.clearItems()
            viewModel.refreshItems(CoroutineContextProvider.Main().get())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gif_listing, container, false)
    }

    private fun initializeDependencies() {
        // inject
        activity?.application?.let {
            TrendingApplication.get(it)
                    .getApplicationComponent()?.plus(ListingModule())?.inject(this)

            // initialize view model
            viewModel =
                    ViewModelProviders.of(this, viewModelFactory).get(ListingViewModel::class.java)
        }
    }

    private fun initializeRecyclerView() {
        rvAdapter = GifListingRecyclerViewAdapter(imageLoader, {
            it?.let { listener?.onItemClicked(it) }
        })
        main_load_more_recycler_view.setOnScrolledToEndListener(object : LoadMoreRecyclerView.OnScrolledToEndListener {
            override fun reachedEnd() {
                viewModel.requestItems(CoroutineContextProvider.Main().get())
            }
        })
        main_load_more_recycler_view.adapter = rvAdapter
        main_load_more_recycler_view.layoutManager = GridLayoutManager(context, 3)
    }

    override fun onResume() {
        super.onResume()

        restoreRecycelrViewStateIfNeeded()
    }

    private fun observeData() {

        // observe on items
        viewModel.gifItems.observe(this, Observer {
            it?.let {
                rvAdapter?.addItems(it)

                rvAdapter?.notifyDataSetChanged()

            }
        })
        // restore recycler view state if needed


        // observe on error
        viewModel.errorMessage.observe(this, Observer {
            Toast.makeText(context, it ?: getString(R.string.general_error), Toast.LENGTH_SHORT)
                    .show()

        })

        // observe on loading
        viewModel.isLoading.observe(this, Observer { loading ->
            if (loading == false) {
                main_progress_bar.visibility = View.GONE
                swipe_refresh_main.isRefreshing = false
            } else {
                main_progress_bar.visibility = View.VISIBLE
            }
        })
        // observe on load more
        viewModel.isLoadingMore.observe(this, Observer { loadingMore ->
            if (loadingMore == false) {
                load_more_progress_bar.visibility = View.GONE
                swipe_refresh_main.isRefreshing = false
            } else {
                load_more_progress_bar.visibility = View.VISIBLE
            }
        })
    }

    private fun restoreRecycelrViewStateIfNeeded() {
        latestLMSate?.let {

            main_load_more_recycler_view.layoutManager?.onRestoreInstanceState(it)

        }

        latestLMSate = null
        /*  if (latestScrollPosition > 0 && rvAdapter?.itemCount ?: 0 > 0) {
              Log.e("latestMState","scrolled to $latestScrollPosition")
              main_load_more_recycler_view.scrollToPosition(latestScrollPosition)
              latestScrollPosition=-1
          }*/
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        main_load_more_recycler_view.layoutManager?.let {
            outState.putParcelable(RECYCLER_VIEW_STATE,
                    it.onSaveInstanceState())


        }
        outState.putInt(LATEST_SCROLL_POSITION, viewModel?.getOffset())
        viewModel.restoreOriginalItems()
    }

    var latestScrollPosition = -1
    var latestLMSate: Parcelable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            val state: Parcelable = it.getParcelable(RECYCLER_VIEW_STATE)
            state.let {
                latestLMSate = it


            }
            latestScrollPosition = it.getInt(LATEST_SCROLL_POSITION, -1)
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface OnFragmentInteractionListener {
        fun onItemClicked(gifUiItem: GifUiItem)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *

         * @return A new instance of fragment GifListingFragment.
         */
        const val TAG = "gif_listing_fragment"
        const val RECYCLER_VIEW_STATE = "r_v_state"
        const val LATEST_SCROLL_POSITION = "latest_scroll_position"
        @JvmStatic
        fun newInstance() =
                GifListingFragment()
    }
}
