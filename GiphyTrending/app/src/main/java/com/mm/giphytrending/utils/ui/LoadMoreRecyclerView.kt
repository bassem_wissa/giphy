package com.mm.giphytrending.utils.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet



class LoadMoreRecyclerView : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attributes: AttributeSet?) : super(context, attributes)
    constructor(context: Context, attributes: AttributeSet?, defStyle: Int) : super(context, attributes, defStyle)

    private var mLinearLayoutManager: LinearLayoutManager? = null
    private var mOnScrolledToEndListener: OnScrolledToEndListener? = null


    private val mOnScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (mLinearLayoutManager == null && layoutManager != null) {
                if (layoutManager is LinearLayoutManager) {
                    mLinearLayoutManager = layoutManager as LinearLayoutManager?
                }
            }
            if (mLinearLayoutManager != null) {
                val visibleItemCount = mLinearLayoutManager!!.childCount
                val totalItemCount = mLinearLayoutManager!!.itemCount
                val firstVisibleItemPosition = mLinearLayoutManager!!.findFirstVisibleItemPosition()
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    if (mOnScrolledToEndListener != null) {
                        mOnScrolledToEndListener!!.reachedEnd()
                    }
                }
            }
        }
    }

    fun setOnScrolledToEndListener(listener: OnScrolledToEndListener) {
        this.removeOnScrollListener(mOnScrollListener)
        this.addOnScrollListener(mOnScrollListener)
        this.mOnScrolledToEndListener = listener
    }

    interface OnScrolledToEndListener {
        fun reachedEnd()
    }

}