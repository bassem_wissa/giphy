package com.mm.giphytrending.listing.domain


data class GifImageItem(var previewGifUrl: String?,
                        var gifUrl: String?,
                        var webUrl: String?,
                        var userName:String?)