package com.mm.giphytrending.utils

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlin.coroutines.experimental.CoroutineContext


sealed class CoroutineContextProvider {
    abstract fun get(): CoroutineContext
    class Main : CoroutineContextProvider() {
        override fun get(): CoroutineContext {
            return UI
        }
    }

    class IO : CoroutineContextProvider() {
        override fun get(): CoroutineContext {
            return CommonPool
        }
    }
}