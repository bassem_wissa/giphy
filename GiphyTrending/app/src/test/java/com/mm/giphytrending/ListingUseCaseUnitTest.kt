package com.mm.giphytrending

import com.mm.giphytrending.listing.domain.GetTrendingGifsUseCase
import com.mm.giphytrending.listing.domain.GetTrendingGifsUseCaseImpl
import com.mm.giphytrending.listing.domain.GifImageItem
import com.mm.giphytrending.listing.domain.TrendingGifsResult
import com.mm.giphytrending.listing.repository.TrendingRepository
import com.mm.giphytrending.listing.repository.TrendingRepositoryImpl
import com.mm.giphytrending.listing.repository.network.GifItem
import com.mm.giphytrending.listing.repository.network.TrendingResponse
import com.mm.giphytrending.listing.repository.network.TrendingService
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.mock.Calls


@RunWith(MockitoJUnitRunner::class)
class ListingUseCaseUnitTest {
    private lateinit var useCase: GetTrendingGifsUseCase

    private lateinit var trendingRepository: TrendingRepository

    @Before
    fun setup() {
        trendingRepository = TrendingRepositoryImpl(object : TrendingService {
            override fun getTrendingGifs(apiKey: String, pageSize: Int, offset: Int): Call<TrendingResponse> {
                return getMockedCall()
            }
        })
        useCase = GetTrendingGifsUseCaseImpl(trendingRepository)


    }

    private fun getMockedCall(): Call<TrendingResponse> {
        return Calls.response(getTrendingResponse())
    }

    @Test
    fun testUseCase() = runBlocking {


        val res = useCase.getTrendingGifs("", 2, 0)
        val testResult = getTestTrendingGifResult()
        assertEquals(res, testResult)
    }


    private fun getTestTrendingGifResult(): TrendingGifsResult<List<GifImageItem>> {
        return com.mm.giphytrending.listing.domain.Success(
                ArrayList<GifImageItem>().apply {
                    add(GifImageItem("previewGif",
                            "downsizedGif",
                            "someUrl",
                            "myUserName"))
                    add(GifImageItem("previewGif2",
                            "downsizedGif2",
                            "someUrl2",
                            "myUserName2"))
                }
        )
    }

    private fun getTrendingResponse(): TrendingResponse {
        return TrendingResponse(
                data = getListOfImages()
        )

    }

    private fun getListOfImages(): List<GifItem> {
        return ArrayList<GifItem>().apply {
            add(GifItem("id1",
                    "someUrl",
                    "myUserName",
                    "someTitle",
                    GifItem.ItemImages(
                            GifItem.ItemImages.Image("previewGif"),
                            GifItem.ItemImages.Image("downsizedGif")
                    )))
            add(GifItem("id2",
                    "someUrl2",
                    "myUserName2",
                    "someTitle2",
                    GifItem.ItemImages(
                            GifItem.ItemImages.Image("previewGif2"),
                            GifItem.ItemImages.Image("downsizedGif2")
                    )))
        }
    }
}