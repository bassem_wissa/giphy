package com.mm.giphytrending.listing.repository.network


sealed class NetworkResult
<out T : Any>

data class Success<out T : Any>(val data: T) : NetworkResult<T>()
data class Failure(val error: Throwable?) : NetworkResult<Nothing>()

inline fun <T : Any> NetworkResult<T>.onSuccess(action: (T) -> Unit): NetworkResult<T> {
    if (this is Success) action(data)
    return this
}

inline fun <T : Any> NetworkResult<T>.onError(action: (Throwable) -> Unit):Any {
    if (this is Failure && error != null) action(error)
    return this
}
