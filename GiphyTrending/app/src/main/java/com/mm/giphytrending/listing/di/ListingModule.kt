package com.mm.giphytrending.listing.di

import com.mm.giphytrending.listing.domain.GetTrendingGifsUseCase
import com.mm.giphytrending.listing.domain.GetTrendingGifsUseCaseImpl
import com.mm.giphytrending.listing.repository.TrendingRepository
import com.mm.giphytrending.listing.repository.TrendingRepositoryImpl
import com.mm.giphytrending.listing.repository.network.TrendingService
import com.mm.giphytrending.listing.ui.model.ListingModel
import com.mm.giphytrending.listing.ui.model.ListingModelImpl
import com.mm.giphytrending.listing.ui.viewmodel.ListingViewModelFactory
import com.mm.giphytrending.utils.NetworkStateHelper
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named


@Module
class ListingModule() {



    @ListingScope
    @Provides
    fun providesTrendingService(retrofit: Retrofit): TrendingService {
        return retrofit.create(TrendingService::class.java)
    }

    @ListingScope
    @Provides
    fun providesTrendingRepository(trendingService: TrendingService): TrendingRepository {
        return TrendingRepositoryImpl(trendingService)
    }

    @ListingScope
    @Provides
    fun providesGetTrendingGifsUseCase(repository: TrendingRepository): GetTrendingGifsUseCase {
        return GetTrendingGifsUseCaseImpl(repository)
    }

    @ListingScope
    @Provides
    fun providesListingModel(useCase: GetTrendingGifsUseCase): ListingModel {
        return ListingModelImpl(useCase)
    }

    @ListingScope
    @Provides
    fun providesListingViewModelFactory(model: ListingModel, networkStateHelper: NetworkStateHelper, @Named("API_KEY") apiKey: String): ListingViewModelFactory {
        return ListingViewModelFactory(model, networkStateHelper, apiKey)
    }
}