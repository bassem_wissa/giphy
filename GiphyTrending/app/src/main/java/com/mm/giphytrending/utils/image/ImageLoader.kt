package com.mm.giphytrending.utils.image

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.Glide


interface ImageLoader {
    fun loadImageUrl(imageView: ImageView, url: String?, @DrawableRes placeholderDrawableId: Int)
    fun loadImageUrl(imageView: ImageView, url: String?)
}

