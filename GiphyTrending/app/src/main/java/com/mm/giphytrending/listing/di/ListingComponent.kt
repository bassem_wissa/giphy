package com.mm.giphytrending.listing.di

import com.mm.giphytrending.listing.ui.GifDialogFragment
import com.mm.giphytrending.listing.ui.GifListingFragment
import dagger.Subcomponent


@ListingScope
@Subcomponent(modules = [ListingModule::class])
interface ListingComponent {
    fun inject(target: GifListingFragment)
    fun inject(target: GifDialogFragment)
}