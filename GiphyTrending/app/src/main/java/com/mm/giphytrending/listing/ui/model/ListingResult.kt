package com.mm.giphytrending.listing.ui.model



sealed class ListingResult
<out T : Any>

data class Success<out T : Any>(val data: T) : ListingResult<T>()
data class Failure(val error: Throwable?) : ListingResult<Nothing>()

inline fun <T : Any> ListingResult<T>.onSuccess(action: (T) -> Unit): ListingResult<T> {
    if (this is Success) action(data)
    return this
}

inline fun <T : Any> ListingResult<T>.onError(action: (Throwable) -> Unit):Any {
    if (this is Failure && error != null) action(error)
    return this
}