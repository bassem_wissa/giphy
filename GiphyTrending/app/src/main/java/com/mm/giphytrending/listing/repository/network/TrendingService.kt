package com.mm.giphytrending.listing.repository.network

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface TrendingService {
    @GET("gifs/trending")
    fun getTrendingGifs(@Query("api_key") apiKey: String,
                        @Query("limit") pageSize: Int,
                        @Query("offset") offset: Int): Call<TrendingResponse>
}