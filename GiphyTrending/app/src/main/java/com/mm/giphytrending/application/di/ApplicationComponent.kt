package com.mm.giphytrending.application.di

import com.mm.giphytrending.listing.di.ListingComponent
import com.mm.giphytrending.listing.di.ListingModule
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun plus(module: ListingModule): ListingComponent
}