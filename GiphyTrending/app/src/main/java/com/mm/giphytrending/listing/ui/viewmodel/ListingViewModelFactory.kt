package com.mm.giphytrending.listing.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.mm.giphytrending.listing.ui.model.ListingModel
import com.mm.giphytrending.utils.NetworkStateHelper


class ListingViewModelFactory(private val listingModel: ListingModel, private val networkStateHelper: NetworkStateHelper, private val apiKey: String)
    : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListingViewModel(listingModel, networkStateHelper, apiKey) as T
    }
}