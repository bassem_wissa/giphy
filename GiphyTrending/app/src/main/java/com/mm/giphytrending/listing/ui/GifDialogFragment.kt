package com.mm.giphytrending.listing.ui

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mm.giphytrending.R
import com.mm.giphytrending.application.TrendingApplication
import com.mm.giphytrending.listing.di.ListingModule
import com.mm.giphytrending.listing.ui.model.GifUiItem
import com.mm.giphytrending.utils.image.ImageLoader
import kotlinx.android.synthetic.main.fragment_gif_dialog.*
import javax.inject.Inject
import android.graphics.drawable.ColorDrawable


class GifDialogFragment : DialogFragment() {
    @Inject
    lateinit var imageLoader: ImageLoader

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.application?.let {
            TrendingApplication.get(it)
                    .getApplicationComponent()?.plus(ListingModule())
                    ?.inject(this)
            arguments?.let {
                val gifUiItem: GifUiItem = it.getParcelable(GIF_UI_ITEM_KEY)

                gifUiItem.let {
                    imageLoader.loadImageUrl(gif_image_view, it.gifUrl)
                }
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gif_dialog, container)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    companion object {
        const val TAG = "gif_dialog_fragment"
        private const val GIF_UI_ITEM_KEY = "gif_ui_item"
        fun newInstance(gifUiItem: GifUiItem): GifDialogFragment {
            return GifDialogFragment()
                    .apply {
                        arguments = Bundle()
                                .apply { putParcelable(GIF_UI_ITEM_KEY, gifUiItem) }
                    }
        }
    }
}