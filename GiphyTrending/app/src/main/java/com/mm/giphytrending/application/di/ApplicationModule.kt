package com.mm.giphytrending.application.di

import android.app.Application
import com.google.gson.GsonBuilder
import com.mm.giphytrending.utils.NetworkStateHelper
import com.mm.giphytrending.utils.NetworkStateHelperImpl
import com.mm.giphytrending.utils.image.GlideImageLoader
import com.mm.giphytrending.utils.image.ImageLoader
import dagger.Module
import dagger.Provides
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application, private val baseUrl: String, private val apiKey: String) {
    @Provides
    @Singleton
    internal fun provideApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    internal fun providesConverterFactory(): Converter.Factory {
        val gson = GsonBuilder().disableHtmlEscaping().create()
        return GsonConverterFactory.create(gson)
    }

    @Singleton
    @Provides
    internal fun providesRetrofit(converterFactory: Converter.Factory): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(converterFactory)
                .baseUrl(baseUrl)
                .build()
    }

    @Singleton
    @Provides
    @Named("API_KEY")
    fun providesApiKey(): String = apiKey


    @Singleton
    @Provides
    internal fun providesNetworkStateHelper(application: Application): NetworkStateHelper {
        return NetworkStateHelperImpl(application)
    }

    @Singleton
    @Provides
    internal fun providesImageLoader(): ImageLoader {
        return GlideImageLoader()
    }
}