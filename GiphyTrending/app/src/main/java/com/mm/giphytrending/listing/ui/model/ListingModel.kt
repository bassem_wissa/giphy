package com.mm.giphytrending.listing.ui.model

import com.mm.giphytrending.listing.domain.*


interface ListingModel {
    suspend fun getTrendingGifs(apiKey: String,
                                pageSize: Int,
                                offset: Int): ListingResult<List<GifUiItem>>
}

class ListingModelImpl(private val useCase: GetTrendingGifsUseCase) : ListingModel {
    override suspend fun getTrendingGifs(apiKey: String,
                                         pageSize: Int,
                                         offset: Int): ListingResult<List<GifUiItem>> {
        var items: ArrayList<GifUiItem>? = null
        var throwable: Throwable? = null
        useCase.getTrendingGifs(apiKey, pageSize, offset)
                .onSuccess {
                    items = ArrayList()
                    items?.addAll(it.map { it.toUiItem() })

                }
                .onError {
                    throwable = it

                }
        items?.let { return Success(it) }
        throwable?.let { return Failure(it) }
        return Failure(Error("Could not load items"))
    }

    private fun GifImageItem.toUiItem(): GifUiItem {
        return GifUiItem(
                this.previewGifUrl,
                this.gifUrl,
                this.previewGifUrl,
                this.userName
        )
    }
}