package com.mm.giphytrending.utils.image

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


class GlideImageLoader : ImageLoader {
    override fun loadImageUrl(imageView: ImageView, url: String?) {
        GlideApp.with(imageView.context)
                .load(url)
                .into(imageView)
    }

    override fun loadImageUrl(imageView: ImageView, url: String?, @DrawableRes placeholderDrawableId: Int) {
        GlideApp.with(imageView.context)
                .load(url)
                .placeholder(placeholderDrawableId)
                .into(imageView)
    }
}

@GlideModule
class MyAppGlideModule : AppGlideModule()